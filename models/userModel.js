const { ObjectId } = require("bson");
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    username : {
		type : String,
		required : [true, "Username is required"]
    },
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	uid : {
		type : String,
		required : [true, "UID is required"]
	},
	tasks : [
		{
			taskName : {
				type : String,
				required : [true, "Task name is required"]
			},
            isComplete : {
				type : String,
				default : false
			},
			timestamp : {
				type : Date,
				default : new Date()
			},

		}
	]
})

module.exports = mongoose.model("User", userSchema);