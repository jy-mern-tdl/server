require('dotenv').config()
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const middleware = require('./middleware/middleware')
const taskRoutes = require("./routes/taskRoutes");
const userRoutes = require("./routes/userRoutes");

// Add the database connection
mongoose.connect(process.env.MONGO_CREDENTIALS, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

// Server setup
const app = express();

const corsOptions = {
	origin: ['http://localhost:3000', 'https://mern-tdl.vercel.app'],
	optionsSuccessStatus: 200
}

app.use(cors(corsOptions));
app.use(middleware.decodeToken);
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//connect routes
app.use("/tasks", taskRoutes);
app.use("/users", userRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
