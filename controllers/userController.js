const User = require("../models/userModel");
const firebaseApp = require('firebase/app');
const firebaseAuth = require('firebase/auth');
const firebaseAdmin = require("firebase-admin");
const credentials = require("../todolist-cbe77-firebase-adminsdk-l4n1v-81118ba0c0.json");
const firebaseConfig = require("../firebase-config.js");

firebaseApp.initializeApp(firebaseConfig)

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(credentials)
});

module.exports.registerUser = async (body) => {
    try{
        let userResponse = await firebaseAdmin.auth().createUser({
            email: body.email,
            password: body.password,
            emailVerified: false,
            disabled: false
        });
    
        if(userResponse){
            let newUser = new User({
                username: body.username,
                email: body.email,
                uid: userResponse.uid
            })

            if(newUser.save()){
                return true
            }else{
                return false
            }
        }
    }

    catch(error){
        return false
    }
}

module.exports.loginUser = (body) => {
    const auth = firebaseAuth.getAuth();
    const accessToken = firebaseAuth.signInWithEmailAndPassword(auth, body.email, body.password)
    .then((userCredential) => {
        return {
            access: userCredential.user.stsTokenManager.accessToken
        }
    })

    return accessToken
}

module.exports.getDetails = (uid) => {

	let user = User.findOne({uid: uid}).then(user => {
        return {
            username: user.username,
            uid: user.uid
        }
	});

    return user;
};