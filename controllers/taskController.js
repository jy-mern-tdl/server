const User = require("../models/userModel");

module.exports.getTasks = (uid) => {

	let tasks = User.findOne({uid: uid}).then(user => {
        return user.tasks;
	});

    return tasks;
};

module.exports.addTask = async (data) => {

	let isUserUpdated = await User.findOne({uid: data.uid}).then(user => {
		// Adds the task name in the user's tasks array
		user.tasks.push({taskName : data.taskName});
		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

    return isUserUpdated;
};

module.exports.updateTask = async (data) => {

    let isTaskUpdated = await User.findOne({uid: data.uid}).then(user => {

        const taskId = user.tasks.map(task => task.id).indexOf(data.taskId)

        user.tasks[taskId].taskName = data.taskName;
        user.tasks[taskId].isComplete = data.isComplete;

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })

    return isTaskUpdated;
}

module.exports.deleteTask = async (data) => {

    let isTaskDeleted = await User.findOne({uid: data.uid}).then(user => {

        const taskId = user.tasks.map(task => task.id).indexOf(data.taskId)

        user.tasks.splice(taskId, 1)

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })

    return isTaskDeleted;
}