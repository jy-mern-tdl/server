const firebaseAdmin = require("firebase-admin");

class Middleware {
	async decodeToken(req, res, next) {
		if(req.headers.authorization){
			let accessToken = req.headers.authorization.slice(7, req.headers.authorization.length);
			try {
	
				const decodedToken = await firebaseAdmin.auth().verifyIdToken(accessToken);
	
				if (decodedToken) {
					req.uid = decodedToken.uid;
					return next();
				}
				return res.json({ message: 'Unauthorized' });
			} catch (e) {
				return res.json({ message: 'Internal Server Error' });
			}
		}else{
			return next();
		}

	}
}

module.exports = new Middleware();