const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

router.get("/get", (req, res) => {
	taskController.getTasks(req.uid).then(resultFromController => res.send(resultFromController));
})

router.post("/add", (req, res) => {
    let data = {
		// User ID will be retrieved from the request header
		uid: req.uid,
		// Task name will be retrieved from the request body
		taskName: req.body.taskName
	}

	taskController.addTask(data).then(resultFromController => res.send(resultFromController));
})

router.put("/update", (req,res) => {

    let data = {
		uid: req.uid,
		// Task name and ID will be retrieved from the request body
		taskId: req.body.taskId,
        taskName: req.body.taskName,
        isComplete: req.body.isComplete
	}

	taskController.updateTask(data).then(resultFromController => res.send(resultFromController));
})

router.delete("/delete", (req,res) => {
    let data = {
		uid: req.uid,
		taskId: req.body.taskId,
	}

	taskController.deleteTask(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;